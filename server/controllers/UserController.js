const Util = require("../Utils");
const http = require('http');
const util = new Util();
class UserController {
    static async processStatistics(req, res) {
        try {
            let startDate = req.query.start ? new Date(req.query.start) : '';
            let endDate = req.query.end ? new Date(req.query.end) : '';
            return http.get("http://45.79.111.106/interview.json", function(response) {
                let data = '',
		        json_data;
                response.on('data', function(stream) {
                    data += stream;
                });
                response.on('end', function() {
                    json_data = JSON.parse(data);
                    if (json_data.length > 0) {
                        if(startDate && endDate){
                            json_data = json_data.filter(function (jsonData) {
                                var date = new Date(jsonData.date);
                                return (date >= startDate && date <= endDate);
                              });
                        }
                        
                        var finalJson = json_data.reduce(function (arr, obj) {
                            var objForId = arr.filter(function (idObj) { return idObj.websiteId === obj.websiteId})[0]
                            
                            if (objForId) {
                              objForId.chats += obj.chats;
                              objForId.missedChats += obj.missedChats;
                            } else {
                                arr.push({
                                    "websiteId": obj.websiteId,
                                    "chats": obj.chats,
                                    "missedChats": obj.missedChats
                                })
                            }
                          
                            return arr;
                          }, [])
                        util.setSuccess(200, "Statistics Details",finalJson);
                    } else {
                        util.setSuccess(200, "No Record Found");
                    }
                    return util.send(res);
                });
            });
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }

}
module.exports = UserController;
