const express = require("express");
const bodyParser = require("body-parser");
const userRoutes = require("./server/routes/userRoutes");
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const port = 8080;
app.use("/api/v1/user", userRoutes);
app.get("*", (req, res) =>
  res.status(200).send({
    message: "Welcome to this API."
  })
);
app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});
module.exports = app;
